import json
import os
import MySQLdb
from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
import paho.mqtt.client as paho

os.environ['SPARK_CLASSPATH'] = "/Users/ismailfaruqi/Documents/jobs/veritrans/hackathon-2015/sample-spark-job/mysql-connector-java-5.0.8-bin.jar"

def send_to_mqtt(record):
  mqtt_client = paho.Client()
  mqtt_client.connect('localhost')
  merchant_id = record[0]['merchant_id']
  payload = json.dumps({ "mid": merchant_id, "type": "TXN_DENY", "count": record[1] }, ensure_ascii=True)
  topic = "/%s" % merchant_id
  mqtt_client.publish(topic, payload)
  mqtt_client.disconnect()

# spark connection
sc = SparkContext("local[2]", "DeniedTransactionsSQL")
sqlctx = SQLContext(sc)

df = sqlctx.load(
  source="jdbc",
  url="jdbc:mysql://localhost:3306/turbo_dev",
  dbtable="turbo_dev.pa_transactions",
  user="root"
  )

df.show()

# transactions = ssc.socketTextStream("localhost", 19999)
# json_transactions = transactions.map(lambda text: json.loads(text))
# denied_transactions = json_transactions.filter(lambda item: item['status'] == "DENIED")
# pairs = denied_transactions.map(lambda item: (item, 1))
# denied_counts = pairs.reduce(lambda x, y: (x[0], x[1] + y[1]))

# # # put the denied count to mqtt
# denied_counts.foreachRDD(lambda rdd: rdd.foreach(send_to_mqtt))

