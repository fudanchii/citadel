import json
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
import paho.mqtt.client as paho

def mqtt_send_summary(record):
  mqtt_client = paho.Client()
  mqtt_client.connect('localhost')
  print record
  payload = json.dumps({ "count": record[1] }, ensure_ascii=True)
  topic = record[0]
  mqtt_client.publish(topic, payload, 0, True)
  mqtt_client.disconnect()

def mqtt_send_denied_list(record):
  mqtt_client = paho.Client()
  mqtt_client.connect('localhost')
  print record
  payload = json.dumps(record, ensure_ascii=True)
  topic = '/' + record['merchant_id'] + '/' + 'DENIED_LIST'
  mqtt_client.publish(topic, payload, 0, True)
  mqtt_client.disconnect()

# spark connection
sc = SparkContext("local[2]", "TransactionSummary")
ssc = StreamingContext(sc, 2)
ssc.checkpoint('/tmp')

transactions = ssc.socketTextStream("localhost", 19999)
json_transactions = transactions.map(lambda text: json.loads(text))

# transaction_summary
pairs = json_transactions.map(lambda item: ('/' + item['merchant_id'] + '/' + item['status'], 1))
summary = pairs.reduceByKeyAndWindow(lambda x, y: x + y, lambda x, y: x - y, 86400, 10)
summary.foreachRDD(lambda rdd: rdd.foreach(mqtt_send_summary))

# print list of denied transactions
json_transactions.filter(lambda item: item['status'] == 'DENIED').foreachRDD(lambda rdd: rdd.foreach(mqtt_send_denied_list))

ssc.start()
ssc.awaitTermination()
