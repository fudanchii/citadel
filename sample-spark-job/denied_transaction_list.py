import json
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
import paho.mqtt.client as paho

def mqtt_send_denied_list(record):
  mqtt_client = paho.Client()
  mqtt_client.connect('localhost')
  print record
  payload = json.dumps(record, ensure_ascii=True)
  topic = '/' + record['merchant_id'] + '/' + 'DENIED_LIST'
  mqtt_client.publish(topic, payload, 0, True)
  mqtt_client.disconnect()

# spark connection
sc = SparkContext("local[2]", "DeniedTransactionList")
ssc = StreamingContext(sc, 1)

transactions = ssc.socketTextStream("localhost", 19999)
json_transactions = transactions.map(lambda text: json.loads(text))
json_transactions \
  .filter(lambda item: item['status'] == 'DENIED') \
  .foreachRDD(lambda rdd: rdd.foreach(mqtt_send_denied_list))

ssc.start()
ssc.awaitTermination()
