'use strict';

window.citadel = {
  URLs: {
    signin: "http://638cafb2.ngrok.com/login.json",
    signout: "http://638cafb2.ngrok.com/signout.json"
  }
};

citadel.init = function () {
};

citadel.loggedIn = function () {
  var loggedIn = localStorage.getItem('loggedIn');
  console.log(loggedIn);
  return loggedIn === '1';
}

citadel.login = function (email, password) {
  console.log(email, password);
  var ajaxOptions = {
    method: "POST",
    contentType: 'application/json',
    data: JSON.stringify({
      user: {
        email: email,
        password: password
      }
    })
  };
  $.ajax(citadel.URLs.signin, ajaxOptions)
  .done(function (data) {
    citadel.setLogin(data);
    console.log(data);
    // storage.saveUser();
  })
  .fail(function (xhr, status, error) {
    var errMsg = xhr.responseJSON && xhr.responseJSON.error;
    citadel.notifyError(errMsg || error || "We just can't login :(");
  });
};

citadel.logout = function () {

  console.log('logging out');
  var ajaxOptions = {
    method: 'DELETE'
  };
  $.ajax(citadel.URLs.signout, ajaxOptions)
  .done(function () {
    citadel.setLogout();
    console.log('logged out');
  })
  .fail(function () {
  });
};

citadel.setLogin = function (data) {
  localStorage.setItem('loggedIn', '1');
  localStorage.setItem('userData', JSON.stringify(data));
  setupDashboard(data);
  citadel.toggleAuthFormVisibility();
  citadel.toggleDashBoardVisibility();
  port.postMessage(JSON.stringify({
    type: 'signin',
    data: data
  }));
};

citadel.setLogout = function (data) {
  localStorage.setItem('loggedIn', '0');
  citadel.toggleDashBoardVisibility();
  citadel.toggleAuthFormVisibility();
  port.postMessage(JSON.stringify({
    type: 'signout',
    data: null
  }));
};

citadel.toggleAuthFormVisibility = function () {
  $('#auth-container')
    .toggleClass('not-visible');
}

citadel.toggleDashBoardVisibility = function () {
  $('#main-container')
    .toggleClass('not-visible');
}

citadel.notifyError = function (msg) {
  console.error(msg);
}

$(citadel.init.bind(window));
