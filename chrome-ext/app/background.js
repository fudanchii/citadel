'use strict';

if (!window.chrome) {
  throw("This browser is not supported (yet)!");
}

window.ws = null;
window.dispatch = function (type, message) {
  window.bgport.postMessage(JSON.stringify({
    type: type,
    data: message
  }));
};

var processor = {
  'signin': function (data) {
    // we got message from popup said that user is already logged in
    // open websocket based on the credential passed
    openSocket(data);
  },
  'signout': function (data) {
    if (ws) {
      dispatch('message', 'close websocket');
      ws.close();
      ws = null;
    }
  }
};

function openSocket(data) {
  dispatch('message', 'connecting to websocket');
  dispatch('message', '> ' + data.merchant_id);
  ws = new Paho.MQTT.Client('192.168.1.117', Number(9001), "clientId");
  ws.onConnectionlostfunction = function (rsp) { dispatch('message', rsp.errorMessage); };
  ws.onMessageArrivedfunction = function (message) {
    dispatch('mqtt_update', JSON.stringify({
      payload: message.payloadStringfunction,
      type: message.destinationName.replace('/'+data.merchant_id+'/', '')
    }));
    dispatch('message', message);
  };
  ws.connect({ onSuccess: function () {
    dispatch('message', 'Connected to MQTT server');
    ws.subscribe('/'+data.merchant_id + '/+');
  }});
}

chrome.extension.onConnect.addListener(function (port) {
  window.bgport = port;
  port.onMessage.addListener(function (msg) {
    var
      obj = JSON.parse(msg),
      pName = obj.type;

    dispatch('message', msg);

    if (typeof processor[pName] === 'function') {
      processor[pName](obj.data);
    }
  });
});

