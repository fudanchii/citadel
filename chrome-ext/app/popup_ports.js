'use strict';

var processor = {
  'message': function (msg) {
    console.log('msg> ', msg);
  },

  'websocket_opened': function (msg) { },

  'mqtt_update': function (msg) {
    var
      obj = JSON.parse(msg);
    switch (obj.type) {
    case 'DENIED_LIST':
      var
        payload = JSON.parse(obj.payload),
        li = $('<li data-transaction_id="'+payload.transaction_id+'"/>')
        .append('<p class="time">' +
                Intl.DateTimeFormat('en-US', {
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: false
                })
                .format(new Date(payload.timestamp))+'</p>')
        .append('<p class="amount">'+Number(payload.amount).toLocaleString()+'</p>')
        .append('<p class="reason">'+payload.reason+'</p>');
      if ($('#js-list-content').children().length == 5) {
        $('#js-list-content').children()[0].remove();
      }
      $('#js-list-content').append(li);
      break;
    default:
      $('#TXN_'+obj.type)
        .prop('number', 0+$('#TXN_'+obj.type).text())
        .animateNumber({ number: JSON.parse(obj.payload).count });
    }
    console.log('mqtt_update> ', msg);
  }
};

window.port = chrome.extension.connect({ name: 'connect to background' });

port.onMessage.addListener(function (msg) {
  var
    obj = JSON.parse(msg),
    pName = obj.type;

  if (typeof window.processor[pName] === 'function') {
    window.processor[pName](obj.data);
    return;
  }

  console.error('Unindentified message type: ' + msg);
});

