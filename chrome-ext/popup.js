'use strict';

if (!window.chrome) {
  throw("This browser is not supported (yet)!");
}

function setupDashboard(data) {
  $('#store-name').text(data.full_name);
}

var onload = function () {
  if (!citadel.loggedIn()) {
    console.log('show auth');
    citadel.toggleAuthFormVisibility();
  } else {
    console.log('show dashboard');
    citadel.toggleDashBoardVisibility();
    var data = JSON.parse(localStorage.getItem('userData'));
    setupDashboard(data);
    port.postMessage(JSON.stringify({
      type: 'signin',
      data: data
    }));
  }
};

$(function () {

  $('#js-see-all').on('click', function (ev) {
    chrome.tabs.create({ url: "https://my.veritrans.co.id" });
  });

  $('#register-button').on('click', function (ev) {
    chrome.tabs.create({ url: "https://my.veritrans.co.id/register" });
  });

  $('#signin-button').on('click', function (ev) {
    citadel.login($('#form-login-email').val(), $('#form-login-password').val());
    //citadel.setLogin();
  });

  $('#sign-out-anchor').on('click', function (ev) {
    citadel.logout();
  });

  onload();

});
